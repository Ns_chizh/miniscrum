// @ts-check

const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const outputDirectory = "dist";

module.exports = {
  entry: path.join(__dirname, "src", "client", "index.js"),
  output: {
    path: path.join(__dirname, outputDirectory),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
            plugins: ["@babel/plugin-proposal-class-properties"]
          }
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  devServer: {
    port: 3000,
    open: true,
    proxy: {
      "/task": "http://localhost:8080"
    }
  },
  plugins: [
    new CleanWebpackPlugin([outputDirectory]),
    new HTMLWebpackPlugin({
      template: path.join(__dirname, "src", "client", "index.html"),
      favicon: path.join(__dirname, "src", "client", "favicon.png")
    }),
    new CopyWebpackPlugin([path.join(__dirname, "src", "client", "style.css")])
  ]
};
