// @ts-check

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Chip from "@material-ui/core/Chip";
import { withStyles } from "@material-ui/core/styles";

import hideError from "../redux/actions/tasks/hideError";

const styles = theme => ({
  errorBlock: {
    margin: "5px",
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap"
  },
  errorItem: {
    margin: "0 5px"
  }
});

class Errors extends Component {
  handleHideError = error => () => {
    this.props.hideError(error.index);
  };

  render() {
    const errors = this.props.errors.filter(error => error.visible);
    const { classes } = this.props;
    if (errors && errors.length > 0) {
      return (
        <div className={classes.errorBlock}>
          {errors.map(error => {
            return (
              <Chip
                key={error.index}
                label={error.message}
                className={classes.errorItem}
                color="secondary"
                onDelete={this.handleHideError(error)}
              />
            );
          })}
        </div>
      );
    } else {
      return null;
    }
  }
}

Errors.propTypes = {
  errors: PropTypes.array
};

const mapStateToProps = state => {
  return {
    errors: state.tasks.errors
  };
};

const mapDispatchToProps = {
  hideError
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Errors));
