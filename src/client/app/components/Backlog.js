// @ts-check

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TaskList from "./TaskList";
import Typography from "@material-ui/core/Typography";

class Backlog extends Component {
  render() {
    const searchTask = this.props.searchTask;
    const tasks = searchTask
      ? this.props.tasks.filter(
          task =>
            task.name.includes(searchTask) ||
            task.description.includes(searchTask)
        )
      : this.props.tasks;

    return (
      <aside id="content__backlog">
        <Typography variant="headline" component="h2">
          Backlog
        </Typography>
        <TaskList tasks={tasks} />
      </aside>
    );
  }
}

Backlog.propTypes = {
  searchTask: PropTypes.string,
  tasks: PropTypes.array
};

const mapStateToProps = (state, props) => {
  return {
    tasks: props.tasks,
    searchTask: state.tasks.search
  };
};

export default connect(mapStateToProps)(Backlog);
