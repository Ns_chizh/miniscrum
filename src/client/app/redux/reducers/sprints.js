// @ts-check

import { SELECT_SPRINT } from "../actions/actionTypes";

const initialState = {
  selected: null
};

const sprints = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_SPRINT: {
      return {
        ...state,
        selected: action.payload
      };
    }
    default: {
      return state;
    }
  }
};

export default sprints;
