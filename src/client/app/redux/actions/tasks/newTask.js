// @ts-check

import { NEW_TASK } from "../actionTypes";

const newTask = () => ({
  type: NEW_TASK
});

export default newTask;
