// @ts-check

import { ADD_TASK, ERROR_ADD_TASKS } from "../actionTypes";

const addTask = (name, sprint, story, description) => {
  return function(dispatch) {
    name = name.trim();
    sprint = sprint.trim().toLowerCase();
    story = story.trim();
    description = description.trim();
    return fetch("/task/add", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({
        name,
        sprint,
        story,
        description
      })
    })
      .then(res => res.json())
      .then(task => {
        dispatch({
          type: ADD_TASK,
          payload: task
        });
      })
      .catch(err => {
        dispatch({
          type: ERROR_ADD_TASKS,
          payload: err.message
        });
      });
  };
};

export default addTask;
