// @ts-check

import { LOAD_TASKS, ERROR_LOAD_TASKS, SELECT_SPRINT } from "../actionTypes";

const loadTasks = () => {
  return function(dispatch) {
    return fetch("/task")
      .then(res => res.json())
      .then(tasks => {
        dispatch({
          type: LOAD_TASKS,
          payload: tasks
        });
        const sprints = Array.from(
          new Set(
            tasks
              .map(task => task.sprint)
              .filter(sprint => sprint !== "")
              .sort()
          )
        );
        dispatch({
          type: SELECT_SPRINT,
          payload: sprints[sprints.length - 1]
        });
      })
      .catch(err => {
        dispatch({
          type: ERROR_LOAD_TASKS,
          payload: err.message
        });
      });
  };
};

export default loadTasks;
