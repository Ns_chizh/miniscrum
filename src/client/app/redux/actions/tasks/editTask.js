// @ts-check

import { EDIT_TASK } from "../actionTypes";

const editTask = task => ({
  type: EDIT_TASK,
  payload: task
});

export default editTask;
