// @ts-check

import { ARCHIVE_TASK, ERROR_UPDATE_TASKS } from "../actionTypes";

const archiveTask = id => {
  return function(dispatch) {
    return fetch(`/task/archive`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify({
        id
      })
    })
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: ARCHIVE_TASK,
          payload: id
        });
      })
      .catch(err => {
        dispatch({
          type: ERROR_UPDATE_TASKS,
          payload: err.message
        });
      });
  };
};

export default archiveTask;
