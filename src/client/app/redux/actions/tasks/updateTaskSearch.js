// @ts-check

import { UPDATE_TASK_SEARCH } from "../actionTypes";

const updateTaskSearch = search_text => ({
  type: UPDATE_TASK_SEARCH,
  payload: search_text
});

export default updateTaskSearch;
