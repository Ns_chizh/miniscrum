// @ts-check

import { SELECT_SPRINT } from "../actionTypes";

const selectSprint = sprint => ({
  type: SELECT_SPRINT,
  payload: sprint
});

export default selectSprint;
