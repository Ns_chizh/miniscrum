// @ts-check

const Datastore = require("nedb");

const { DB_FILE_PATH } = require("./config");

const DataBase = class DataBaseNeDBHelper {
  /**
   *
   * @param {String} path Path to database file
   */
  constructor(path) {
    this.db = new Datastore(path);
    this.db.loadDatabase();
  }

  loadTasks() {
    return new Promise((resolve, reject) => {
      this.db.find({}, function(err, records) {
        if (err) {
          reject(err);
        } else {
          resolve(records);
        }
      });
    });
  }

  /**
   *
   * @param {String} id
   */
  loadTask(id) {
    return new Promise((resolve, reject) => {
      this.db.findOne({ _id: id }, function(err, records) {
        if (err) {
          reject(err);
        } else {
          resolve(records);
        }
      });
    });
  }

  /**
   *
   * @param {String} name
   * @param {String} sprint
   * @param {String} story
   * @param {String} description
   */
  insertTask(name, sprint, story, description) {
    return new Promise((resolve, reject) => {
      this.db.insert(
        {
          name,
          sprint,
          story,
          description
        },
        function(err, record) {
          if (err) {
            reject(err);
          } else {
            resolve(record);
          }
        }
      );
    });
  }

  /**
   *
   * @param {String} id
   * @param {String} name
   * @param {String} sprint
   * @param {String} story
   * @param {String} description
   */
  updateTask(id, name, sprint, story, description) {
    return new Promise((resolve, reject) => {
      this.db.update(
        {
          _id: id
        },
        {
          name,
          sprint,
          story,
          description
        },
        {},
        function(err, record) {
          if (err) {
            reject(err);
          } else {
            resolve(record);
          }
        }
      );
    });
  }

  /**
   *
   * @param {String} id
   */
  deleteTask(id) {
    return new Promise((resolve, reject) => {
      this.db.remove({ _id: id }, {}, function(err, numRemoved) {
        if (err) {
          reject(err);
        } else {
          resolve(numRemoved);
        }
      });
    });
  }

  countTasks() {
    return new Promise((resolve, reject) => {
      this.db.count({}, function(err, count) {
        if (err) {
          reject(err);
        } else {
          resolve(count);
        }
      });
    });
  }
};

module.exports = new DataBase(DB_FILE_PATH);
