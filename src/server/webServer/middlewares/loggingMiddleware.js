// @ts-check

const logger = require("../../logger");

const loggingMiddleware = function loggingRequestPathMiddleware(
  req,
  res,
  next
) {
  logger.info(`Request path ${req.path}}`);
  next();
};

module.exports = loggingMiddleware;
