// @ts-check

const express = require("express");
var bodyParser = require("body-parser");

const logger = require("../logger");

function setupStaticSite(app, staticSitePath) {
  app.use(express.static(staticSitePath));
}

function setupRouter(app) {
  const tasksController = require("../controllers/tasksController");
  const taskController = require("../controllers/taskCotroller");
  const addTaskController = require("../controllers/addTaskController");
  const updateTaskController = require("../controllers/updateTaskController");
  const archiveTaskController = require("../controllers/archiveTaskController");
  const deleteTaskController = require("../controllers/deleteTaskController");

  const jsonParse = bodyParser.json();

  app.get("/task", tasksController());
  app.get("/task/:id", taskController());
  app.post("/task/add", jsonParse, addTaskController());
  app.post("/task/update", jsonParse, updateTaskController());
  app.post("/task/archive", jsonParse, archiveTaskController());
  app.delete("/task/delete", jsonParse, deleteTaskController());
}

const runWebServer = function setupAndRunWebServer(port, staticSitePath) {
  const app = express();

  app.listen(port, () => logger.info(`Web server is running on port ${port}`));

  app.use(require("./middlewares/loggingMiddleware"));

  setupStaticSite(app, staticSitePath);

  setupRouter(app);
};

module.exports = runWebServer;
