// @ts-check

const logger = require("../logger");
const database = require("../database");

const controller = function loadAllTaskController() {
  function controller(req, res) {
    database
      .loadTasks()
      .then(tasks => {
        res.send(tasks);
      })
      .catch(reason => {
        const error = {
          message: "Load tasks error"
        };
        logger.error(`${error.messageError}. Error: ${reason}`);
        res.status(500).send(error);
      });
  }
  return controller;
};

module.exports = controller;
