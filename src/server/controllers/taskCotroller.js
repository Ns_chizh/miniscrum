// @ts-check

const logger = require("../logger");
const database = require("../database");

const controller = function loadTaskController() {
  function controller(req, res) {
    let id = req.params.id;
    database
      .loadTask(id)
      .then(task => {
        if (!task) {
          const error = {
            message: `Task with id ${id} not found`
          };
          res.status(404).send(error);
        } else {
          res.send(task);
        }
      })
      .catch(reason => {
        const error = {
          message: `Load task (id: ${id}) error`
        };
        logger.error(`${error.message}. Error: ${reason}`);
        res.status(500).send(error);
      });
  }
  return controller;
};

module.exports = controller;
