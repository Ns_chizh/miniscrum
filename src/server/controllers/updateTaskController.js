// @ts-check

const logger = require("../logger");
const database = require("../database");
const taskValidator = require("../../shared/validation/taskValidator");

const controller = function updateTaskController() {
  function controller(req, res) {
    let id = req.body.id;
    let name = req.body.name;
    let sprint = req.body.sprint;
    let story = req.body.story;
    let description = req.body.description;
    const validation = taskValidator({ name, sprint, story });
    if (validation.size > 0) {
      res.status(404).send(validation);
    } else {
      database
        .updateTask(id, name, sprint, story, description)
        .then(countUpdated => {
          if (countUpdated === 0) {
            const error = {
              message: `Task with id ${id} not found`
            };
            res.status(404).send(error);
          } else {
            res.status(200).send({ message: `Task with id ${id} updated` });
          }
        })
        .catch(reason => {
          const error = {
            message: `Error update task id: ${id}`
          };
          logger.error(`${error.message}. Error: ${reason}`);
          res.status(500).send(error);
        });
    }
  }
  return controller;
};

module.exports = controller;
