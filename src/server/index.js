// @ts-check

const logger = require("./logger");
const runWebServer = require("./webServer/runWebServer");
const { WEBSERVER_PORT, WEBSERVER_PUBLIC_PATH } = require("./config");

logger.info("miniScrum v1.0.0");

logger.info("Load database");

require("./database");

logger.info("Run web server");

runWebServer(WEBSERVER_PORT, WEBSERVER_PUBLIC_PATH);
